package ru.lae.downloadSite;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

/**
 * Класс для скачивания страницы
 *
 * @author Lukashevich A.
 */
class Downloading implements Const{
    Downloading(String src, String name) {
        try {
            download(src, name);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Скачивание страницы
     *
     * @param src  - сслыка на страницу
     * @param name - имя файла
     * @throws IOException - Исключения
     */
    private void download(String src, String name) throws IOException {
        URL url = new URL(src);
        FileOutputStream outFile;

        try (ReadableByteChannel byteChannelForDownload = Channels.newChannel(url.openStream())) {
            outFile = new FileOutputStream(name + HTML);
            outFile.getChannel().transferFrom(byteChannelForDownload, 0, Long.MAX_VALUE);
        }
    }
}
